/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "audio_render_vendor.h"

#include <hdf_base.h>
#include <limits.h>
#include "audio_common_vendor.h"
#include "audio_uhdf_log.h"
#include "osal_mem.h"
#include "securec.h"

#define HDF_LOG_TAG    HDF_AUDIO_PRIMARY_IMPL

struct AudioRenderInfo {
    struct IAudioRender render;
    struct AudioDeviceDescriptor desc;
    enum AudioCategory streamType;
    unsigned int sampleRate;
    unsigned int channelCount;
    struct AudioHwiRender *hwiRender;
    uint32_t renderId;
};

struct AudioHwiRenderPriv {
    struct AudioRenderInfo *renderInfos[AUDIO_HW_STREAM_NUM_MAX];
    uint32_t renderCnt;
    struct IAudioCallback *callback;
    bool isRegCb;
};

static struct AudioHwiRenderPriv g_audioHwiRenderPriv;

static struct AudioHwiRenderPriv *AudioHwiRenderGetPriv(void)
{
    return &g_audioHwiRenderPriv;
}


struct AudioHwiRender *AudioHwiGetHwiRenderById(uint32_t renderId)
{
    struct AudioHwiRenderPriv *priv = AudioHwiRenderGetPriv();
    if (priv->renderInfos[renderId] == NULL) {
        AUDIO_FUNC_LOGE("not match render");
        return NULL;
    }

    return priv->renderInfos[renderId]->hwiRender;
}

int32_t AudioHwiGetLatency(struct IAudioRender *render, uint32_t *ms)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(ms, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->GetLatency, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->GetLatency(hwiRender, ms);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio GetLatency fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderFrame(struct IAudioRender *render, const int8_t *frame, uint32_t frameLen, uint64_t *replyBytes)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(frame, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(replyBytes, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->RenderFrame, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->RenderFrame(hwiRender, frame, frameLen, replyBytes);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render frame fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiGetRenderPosition(struct IAudioRender *render, uint64_t *frames, struct AudioTimeStamp *time)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(frames, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(time, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->GetRenderPosition, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->GetRenderPosition(hwiRender, frames, (struct AudioHwiTimeStamp *)time);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render, get position fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiSetRenderSpeed(struct IAudioRender *render, float speed)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->SetRenderSpeed, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->SetRenderSpeed(hwiRender, speed);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render SetRenderSpeed fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiGetRenderSpeed(struct IAudioRender *render, float *speed)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(speed, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->GetRenderSpeed, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->GetRenderSpeed(hwiRender, speed);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetRenderSpeed fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderSetChannelMode(struct IAudioRender *render, enum AudioChannelMode mode)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->SetChannelMode, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->SetChannelMode(hwiRender, (enum AudioHwiChannelMode)mode);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio SetChannelMode fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetChannelMode(struct IAudioRender *render, enum AudioChannelMode *mode)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(mode, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->GetChannelMode, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->GetChannelMode(hwiRender, (enum AudioHwiChannelMode *)mode);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetChannelMode fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

static int32_t AudioHwiRenderHwiCallback(enum AudioHwiCallbackType type, void *reserved, void *cookie)
{
    CHECK_NULL_PTR_RETURN_VALUE(reserved, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(cookie, HDF_ERR_INVALID_PARAM);

    struct AudioHwiRenderPriv *priv = AudioHwiRenderGetPriv();
    struct IAudioCallback *cb = priv->callback;
    int32_t ret = cb->RenderCallback(cb, (enum AudioCallbackType)type, reserved, cookie);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render call RenderCallback fail, ret=%{public}d", ret);
        return HDF_FAILURE;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderRegCallback(struct IAudioRender *render, struct IAudioCallback *audioCallback, int8_t cookie)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(audioCallback, HDF_ERR_INVALID_PARAM);

    struct AudioHwiRenderPriv *priv = AudioHwiRenderGetPriv();
    if (priv->isRegCb) {
        AUDIO_FUNC_LOGI("audio render call RegCallback have registered");
        return HDF_SUCCESS;
    }

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->RegCallback, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->RegCallback(hwiRender, AudioHwiRenderHwiCallback, &cookie);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render call RegCallback fail, ret=%{public}d", ret);
        return HDF_FAILURE;
    }

    priv->callback = audioCallback;
    priv->isRegCb = true;

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderDrainBuffer(struct IAudioRender *render, enum AudioDrainNotifyType *type)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(type, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->DrainBuffer, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->DrainBuffer(hwiRender, (enum AudioHwiDrainNotifyType *)type);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render DrainBuffer fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderIsSupportsDrain(struct IAudioRender *render, bool *support)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(support, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->IsSupportsDrain, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->IsSupportsDrain(hwiRender, support);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render IsSupportsDrain fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderCheckSceneCapability(struct IAudioRender *render, const struct AudioSceneDescriptor *scene,
    bool *supported)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(scene, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(supported, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->scene.CheckSceneCapability, HDF_ERR_INVALID_PARAM);

    struct AudioHwiSceneDescriptor hwiScene;
    (void)memset_s((void *)&hwiScene, sizeof(hwiScene), 0, sizeof(hwiScene));
    int32_t ret = AudioHwiCommonSceneToHwiScene(scene, &hwiScene);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render scene To hwiScene fail");
        return HDF_FAILURE;
    }

    ret = hwiRender->scene.CheckSceneCapability(hwiRender, &hwiScene, supported);
    OsalMemFree((void *)hwiScene.desc.desc);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render CheckSceneCapability fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderSelectScene(struct IAudioRender *render, const struct AudioSceneDescriptor *scene)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(scene, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->scene.SelectScene, HDF_ERR_INVALID_PARAM);

    struct AudioHwiSceneDescriptor hwiScene;
    (void)memset_s((void *)&hwiScene, sizeof(hwiScene), 0, sizeof(hwiScene));
    int32_t ret = AudioHwiCommonSceneToHwiScene(scene, &hwiScene);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render scene To hwiScene fail");
        return HDF_FAILURE;
    }

    ret = hwiRender->scene.SelectScene(hwiRender, &hwiScene);
    OsalMemFree((void *)hwiScene.desc.desc);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render select scene fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderSetMute(struct IAudioRender *render, bool mute)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->volume.SetMute, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->volume.SetMute(hwiRender, mute);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render SetMute fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetMute(struct IAudioRender *render, bool *mute)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(mute, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->volume.GetMute, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->volume.GetMute(hwiRender, mute);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetMute fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderSetVolume(struct IAudioRender *render, float volume)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->volume.SetVolume, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->volume.SetVolume(hwiRender, volume);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render SetVolume fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetVolume(struct IAudioRender *render, float *volume)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(volume, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->volume.GetVolume, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->volume.GetVolume(hwiRender, volume);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetVolume fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetGainThreshold(struct IAudioRender *render, float *min, float *max)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(min, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(max, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->volume.GetGainThreshold, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->volume.GetGainThreshold(hwiRender, min, max);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetGainThreshold fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetGain(struct IAudioRender *render, float *gain)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(gain, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->volume.GetGain, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->volume.GetGain(hwiRender, gain);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetGain fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderSetGain(struct IAudioRender *render, float gain)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->volume.SetGain, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->volume.SetGain(hwiRender, gain);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render SetGain fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetFrameSize(struct IAudioRender *render, uint64_t *size)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(size, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.GetFrameSize, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->attr.GetFrameSize(hwiRender, size);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetFrameSize fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetFrameCount(struct IAudioRender *render, uint64_t *count)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(count, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.GetFrameCount, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->attr.GetFrameCount(hwiRender, count);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetFrameCount fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderSetSampleAttributes(struct IAudioRender *render, const struct AudioSampleAttributes *attrs)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(attrs, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.SetSampleAttributes, HDF_ERR_INVALID_PARAM);

    struct AudioHwiSampleAttributes hwiAttrs;
    (void)memset_s((void *)&hwiAttrs, sizeof(hwiAttrs), 0, sizeof(hwiAttrs));
    int32_t ret = AudioHwiCommonSampleAttrToHwiSampleAttr(attrs, &hwiAttrs);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render SampleAttr to hwisampleAttr fail, ret=%{public}d", ret);
        return ret;
    }

    ret = hwiRender->attr.SetSampleAttributes(hwiRender, &hwiAttrs);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render SetSampleAttributes fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetSampleAttributes(struct IAudioRender *render, struct AudioSampleAttributes *attrs)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(attrs, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.GetSampleAttributes, HDF_ERR_INVALID_PARAM);

    struct AudioHwiSampleAttributes hwiAttrs;
    (void)memset_s((void *)&hwiAttrs, sizeof(hwiAttrs), 0, sizeof(hwiAttrs));
    int32_t ret = hwiRender->attr.GetSampleAttributes(hwiRender, &hwiAttrs);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetSampleAttributes fail, ret=%{public}d", ret);
        return ret;
    }

    ret = AudioHwiCommonHwiSampleAttrToSampleAttr(&hwiAttrs, attrs);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render hwiSampleAttr to SampleAttr fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetCurrentChannelId(struct IAudioRender *render, uint32_t *channelId)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(channelId, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.GetCurrentChannelId, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->attr.GetCurrentChannelId(hwiRender, channelId);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetCurrentChannelId fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderSetExtraParams(struct IAudioRender *render, const char *keyValueList)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(keyValueList, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.SetExtraParams, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->attr.SetExtraParams(hwiRender, keyValueList);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render SetExtraParams fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetExtraParams(struct IAudioRender *render, char *keyValueList, uint32_t keyValueListLen)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(keyValueList, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.GetExtraParams, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->attr.GetExtraParams(hwiRender, keyValueList, keyValueListLen);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetExtraParams fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderReqMmapBuffer(struct IAudioRender *render, int32_t reqSize,
    struct AudioMmapBufferDescriptor *desc)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(desc, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.ReqMmapBuffer, HDF_ERR_INVALID_PARAM);

    struct AudioHwiMmapBufferDescriptor hwiDesc = {0};
    int32_t ret = hwiRender->attr.ReqMmapBuffer(hwiRender, reqSize, &hwiDesc);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render ReqMmapBuffer fail, ret=%{public}d", ret);
        return ret;
    }

    desc->memoryAddress = NULL;
    desc->memoryFd = hwiDesc.memoryFd;
    desc->totalBufferFrames = hwiDesc.totalBufferFrames;
    desc->transferFrameSize = hwiDesc.transferFrameSize;
    desc->isShareable = hwiDesc.isShareable;
    desc->offset = hwiDesc.offset;
    desc->filePath = strdup("");

    AUDIO_FUNC_LOGI("%{public}s success", __func__);
    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetMmapPosition(struct IAudioRender *render, uint64_t *frames, struct AudioTimeStamp *time)
{
    struct AudioHwiTimeStamp hwiTime;
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(frames, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(time, HDF_ERR_INVALID_PARAM);

    hwiTime.tvSec = 0;
    hwiTime.tvNSec = 0;

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.GetMmapPosition, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->attr.GetMmapPosition(hwiRender, frames, &hwiTime);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetMmapPosition fail, ret=%{public}d", ret);
        return ret;
    }

    time->tvSec = hwiTime.tvSec;
    time->tvNSec = hwiTime.tvNSec;

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderAddAudioEffect(struct IAudioRender *render, uint64_t effectid)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.AddAudioEffect, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->attr.AddAudioEffect(hwiRender, effectid);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render AddAudioEffect fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderRemoveAudioEffect(struct IAudioRender *render, uint64_t effectid)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.RemoveAudioEffect, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->attr.RemoveAudioEffect(hwiRender, effectid);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render RemoveAudioEffect fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetFrameBufferSize(struct IAudioRender *render, uint64_t *bufferSize)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(bufferSize, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->attr.GetFrameBufferSize, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->attr.GetFrameBufferSize(hwiRender, bufferSize);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render GetFrameBufferSize fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderStart(struct IAudioRender *render)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->control.Start, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->control.Start(hwiRender);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render Start fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderStop(struct IAudioRender *render)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->control.Stop, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->control.Stop(hwiRender);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render Stop fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderPause(struct IAudioRender *render)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->control.Pause, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->control.Pause(hwiRender);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render Pause fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderResume(struct IAudioRender *render)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->control.Resume, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->control.Resume(hwiRender);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render Resume fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderFlush(struct IAudioRender *render)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->control.Flush, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->control.Flush(hwiRender);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render Flush fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderTurnStandbyMode(struct IAudioRender *render)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->control.TurnStandbyMode, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->control.TurnStandbyMode(hwiRender);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render TurnStandbyMode fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderAudioDevDump(struct IAudioRender *render, int32_t range, int32_t fd)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->control.AudioDevDump, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->control.AudioDevDump(hwiRender, range, fd);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render AudioDevDump fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderIsSupportsPauseAndResume(struct IAudioRender *render, bool *supportPause, bool *supportResume)
{
    CHECK_NULL_PTR_RETURN_VALUE(render, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(supportPause, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(supportResume, HDF_ERR_INVALID_PARAM);

    struct AudioRenderInfo *renderInfo = (struct AudioRenderInfo *)render;
    struct AudioHwiRender *hwiRender = renderInfo->hwiRender;
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(hwiRender->control.IsSupportsPauseAndResume, HDF_ERR_INVALID_PARAM);

    int32_t ret = hwiRender->control.IsSupportsPauseAndResume(hwiRender, supportPause, supportResume);
    if (ret != HDF_SUCCESS) {
        AUDIO_FUNC_LOGE("audio render IsSupportsPauseAndResume fail, ret=%{public}d", ret);
        return ret;
    }

    return HDF_SUCCESS;
}

int32_t AudioHwiRenderGetVersion(struct IAudioRender *render, uint32_t *majorVer, uint32_t *minorVer)
{
    (void)render;
    CHECK_NULL_PTR_RETURN_VALUE(majorVer, HDF_ERR_INVALID_PARAM);
    CHECK_NULL_PTR_RETURN_VALUE(minorVer, HDF_ERR_INVALID_PARAM);

    *majorVer = IAUDIO_RENDER_MAJOR_VERSION;
    *minorVer = IAUDIO_RENDER_MINOR_VERSION;

    return HDF_SUCCESS;
}

static void AudioHwiInitRenderInstance(struct IAudioRender *render)
{
    render->GetLatency = AudioHwiGetLatency;
    render->RenderFrame = AudioHwiRenderFrame;
    render->GetRenderPosition = AudioHwiGetRenderPosition;
    render->SetRenderSpeed = AudioHwiSetRenderSpeed;
    render->GetRenderSpeed = AudioHwiGetRenderSpeed;
    render->SetChannelMode = AudioHwiRenderSetChannelMode;
    render->GetChannelMode = AudioHwiRenderGetChannelMode;
    render->RegCallback = AudioHwiRenderRegCallback;
    render->DrainBuffer = AudioHwiRenderDrainBuffer;
    render->IsSupportsDrain = AudioHwiRenderIsSupportsDrain;
    render->CheckSceneCapability = AudioHwiRenderCheckSceneCapability;
    render->SelectScene = AudioHwiRenderSelectScene;
    render->SetMute = AudioHwiRenderSetMute;
    render->GetMute = AudioHwiRenderGetMute;
    render->SetVolume = AudioHwiRenderSetVolume;
    render->GetVolume = AudioHwiRenderGetVolume;
    render->GetGainThreshold = AudioHwiRenderGetGainThreshold;
    render->GetGain = AudioHwiRenderGetGain;
    render->SetGain = AudioHwiRenderSetGain;
    render->GetFrameSize = AudioHwiRenderGetFrameSize;
    render->GetFrameCount = AudioHwiRenderGetFrameCount;
    render->SetSampleAttributes = AudioHwiRenderSetSampleAttributes;
    render->GetSampleAttributes = AudioHwiRenderGetSampleAttributes;
    render->GetCurrentChannelId = AudioHwiRenderGetCurrentChannelId;
    render->SetExtraParams = AudioHwiRenderSetExtraParams;
    render->GetExtraParams = AudioHwiRenderGetExtraParams;
    render->ReqMmapBuffer = AudioHwiRenderReqMmapBuffer;
    render->GetMmapPosition = AudioHwiRenderGetMmapPosition;
    render->AddAudioEffect = AudioHwiRenderAddAudioEffect;
    render->RemoveAudioEffect = AudioHwiRenderRemoveAudioEffect;
    render->GetFrameBufferSize = AudioHwiRenderGetFrameBufferSize;
    render->Start = AudioHwiRenderStart;
    render->Stop = AudioHwiRenderStop;
    render->Pause = AudioHwiRenderPause;
    render->Resume = AudioHwiRenderResume;
    render->Flush = AudioHwiRenderFlush;
    render->TurnStandbyMode = AudioHwiRenderTurnStandbyMode;
    render->AudioDevDump = AudioHwiRenderAudioDevDump;
    render->IsSupportsPauseAndResume = AudioHwiRenderIsSupportsPauseAndResume;
    render->GetVersion = AudioHwiRenderGetVersion;
}

struct IAudioRender *FindRenderCreated(enum AudioPortPin pin, const struct AudioSampleAttributes *attrs,
    uint32_t *rendrId)
{
    uint32_t index = 0;
    struct AudioHwiRenderPriv *renderPriv = AudioHwiRenderGetPriv();
    if (renderPriv == NULL) {
        AUDIO_FUNC_LOGE("Parameter error!");
        return NULL;
    }

    if (renderPriv->renderCnt == 0) {
        AUDIO_FUNC_LOGI("no render created");
        return NULL;
    }

    for (index = 0; index < AUDIO_HW_STREAM_NUM_MAX; index++) {
        if ((renderPriv->renderInfos[index] != NULL) &&
            (renderPriv->renderInfos[index]->desc.pins == pin) &&
            (renderPriv->renderInfos[index]->streamType == attrs->type) &&
            (renderPriv->renderInfos[index]->sampleRate == attrs->sampleRate) &&
            (renderPriv->renderInfos[index]->channelCount == attrs->channelCount)) {
            *rendrId = renderPriv->renderInfos[index]->renderId;
            return &renderPriv->renderInfos[index]->render;
        }
    }

    return NULL;
}

static uint32_t GetAvailableRenderId(struct AudioHwiRenderPriv *renderPriv)
{
    uint32_t renderId = AUDIO_HW_STREAM_NUM_MAX;
    uint32_t index = 0;
    if (renderPriv == NULL) {
        AUDIO_FUNC_LOGE("Parameter error!");
        return renderId;
    }

    if (renderPriv->renderCnt < AUDIO_HW_STREAM_NUM_MAX) {
        renderId = renderPriv->renderCnt;
        renderPriv->renderCnt++;
    } else {
        for (index = 0; index < AUDIO_HW_STREAM_NUM_MAX; index++) {
            if (renderPriv->renderInfos[index] == NULL) {
                renderId = index;
                break;
            }
        }
    }

    return renderId;
}

struct IAudioRender *AudioHwiCreateRenderById(const struct AudioSampleAttributes *attrs, uint32_t *renderId,
    struct AudioHwiRender *hwiRender, const struct AudioDeviceDescriptor *desc)
{
    struct IAudioRender *render = NULL;
    if (attrs == NULL || renderId == NULL || hwiRender == NULL || desc == NULL) {
        AUDIO_FUNC_LOGE("audio render is null");
        return NULL;
    }

    *renderId = AUDIO_HW_STREAM_NUM_MAX;
    struct AudioHwiRenderPriv *priv = AudioHwiRenderGetPriv();

    *renderId = GetAvailableRenderId(priv);
    if (*renderId >= AUDIO_HW_STREAM_NUM_MAX) {
        AUDIO_FUNC_LOGE("audio hwiRender create render index fail, renderId=%{public}d", *renderId);
        return NULL;
    }

    priv->renderInfos[*renderId] = (struct AudioRenderInfo *)OsalMemCalloc(sizeof(struct AudioRenderInfo));
    if (priv->renderInfos[*renderId] == NULL) {
        AUDIO_FUNC_LOGE("audio HwiRender malloc renderInfos fail");
        return NULL;
    }

    priv->renderInfos[*renderId]->hwiRender = hwiRender;
    priv->renderInfos[*renderId]->streamType = attrs->type;
    priv->renderInfos[*renderId]->sampleRate = attrs->sampleRate;
    priv->renderInfos[*renderId]->channelCount = attrs->channelCount;
    priv->renderInfos[*renderId]->desc.portId = desc->portId;
    priv->renderInfos[*renderId]->desc.pins = desc->pins;
    priv->renderInfos[*renderId]->desc.desc = strdup(desc->desc);
    priv->renderInfos[*renderId]->renderId = *renderId;
    render = &(priv->renderInfos[*renderId]->render);
    AudioHwiInitRenderInstance(render);

    AUDIO_FUNC_LOGI("audio create render success");
    return render;
};

void AudioHwiDestroyRenderById(uint32_t renderId)
{
    if (renderId >= AUDIO_HW_STREAM_NUM_MAX) {
        AUDIO_FUNC_LOGE("audio hwiRender destroy render index fail, descIndex=%{public}d", renderId);
        return;
    }
    struct AudioHwiRenderPriv *priv = AudioHwiRenderGetPriv();    
    if (priv->renderInfos[renderId] == NULL) {
        AUDIO_FUNC_LOGE("audio hwiRender destroy render index fail, descIndex=%{public}d", renderId);
        return;
    }

    OsalMemFree((void *)priv->renderInfos[renderId]->desc.desc);
    priv->renderInfos[renderId]->hwiRender = NULL;
    priv->renderInfos[renderId]->desc.desc = NULL;
    priv->renderInfos[renderId]->desc.portId = UINT_MAX;
    priv->renderInfos[renderId]->desc.pins = PIN_NONE;

    OsalMemFree(priv->renderInfos[renderId]);
    priv->renderInfos[renderId] = NULL;
}
